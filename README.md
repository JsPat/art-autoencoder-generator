# Art-Autoencoder-Generator

Do art (the autoencoder way)

***

## Installation

To install requirements use these commands:

- `virtualenv env`
- `env\Scripts\activate`
- `pip install -r requirements.txt`

***

## Usage

- `cd src`
- `python art_generator.py`

***

## Authors and acknowledgment
Jean-Sébastien Patenaude

***

## License
Apache License 2.0

***

## Project status
Tryout Phase


